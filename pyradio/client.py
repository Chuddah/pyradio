from argparse import ArgumentParser
from player import daemonPlayer
from os import path, getenv
import csv
import sys
import iheart
import socket

DEFAULT_FILE = ''
for p in [path.join(getenv('HOME', '~'), '.pyradio', 'stations.csv'),
          path.join(getenv('HOME', '~'), '.pyradio'),
          path.join(path.dirname(__file__), 'stations.csv')]:
    if path.exists(p) and path.isfile(p):
        DEFAULT_FILE = p
        break

def shell():
    parser = ArgumentParser(description="Client for radio player daemon.")
    parser.add_argument("--status",            action='store_true', help="Display the current status of the player.")
    parser.add_argument("--list",        "-l", action='store_true', help="List of added stations.")
    parser.add_argument("--play",        "-p", nargs='?', default=False, help="Start and play. The value is num station or empty for random.")
    parser.add_argument("--pause",       "-P", action='store_true', default=False, help="Toggles between play and pause.")
    parser.add_argument("--mute",        "-m", action='store_true', default=False, help="Mutes the currently playing stream.")
    parser.add_argument("--stop",        "-S", action='store_true', default=False, help="Stops the currently playing stream.")
    parser.add_argument("--volume-up",   "-V", action='store_true', default=False, help="Adjust the volume up.")
    parser.add_argument("--volume-down", "-v", action='store_true', default=False, help="Adjust the volume down.")
    parser.add_argument("--stations",    "-s", default=DEFAULT_FILE, help="Path on stations csv file.")

    parser.add_argument("--add",         "-a", action='store_true', help="Add station to list.")
    parser.add_argument("--remove",      "-r", nargs='?', default=False, help="Remove a station from the list.")
    parser.add_argument("--search",            action='store', default=None, help="Search iHeart for stations and display the results.")
    parser.add_argument("--search-add",        action='store', default=None, help="Search iHeart and add station to list.")
    args = parser.parse_args()

    player = daemonPlayer()
    try:
        player.ping()
    except socket.error:
        print "Could not establish a connection to the radio daemon."
        exit(1)

    if args.add:
        params = raw_input("Enter the name: "), raw_input("Enter the url: ")
        with open(args.stations, 'a') as cfgfile:
            writter = csv.writer(cfgfile)
            writter.writerow(params)
            sys.exit(0)

    if args.remove:
        with open(args.stations, 'r') as cfgfile:
            reader = csv.reader(cfgfile)
            stations = [row for row in reader]

        remove_id = int(args.remove) - 1
        with open(args.stations, 'w+') as cfgfile:
            writter = csv.writer(cfgfile)
            for index, station in enumerate(stations):
                if remove_id != index:
                    writter.writerow(station)
        sys.exit(0)

    if args.search:
        read_file = iheart.search_cvs(args.search)
        reader = csv.reader(read_file)
        for row in reader:
            print row
        sys.exit(0)

    if args.search_add:
        read_file = iheart.search_cvs(args.search_add)
        reader = csv.reader(read_file)

        with open(args.stations, 'a') as cfgfile:
            writter = csv.writer(cfgfile)
            for row in reader:
                writter.writerow(row)
            sys.exit(0)

    with open(args.stations, 'r') as cfgfile:
        stations = []
        for row in csv.reader(filter(lambda row: row[0]!='#', cfgfile), skipinitialspace=True):
            name, url = [s.strip() for s in row]
            stations.append((name, url))

    if args.list:
        for name, url in stations:
            print(('{0:50s} {1:s}'.format(name, url)))
        sys.exit(0)

    if args.play:
        stream_lookup = dict(stations)
        try:
            stream_url = stream_lookup[args.play]
        except KeyError:
            print 'Unknown station: %s\n' % args.play
            print 'Known stations:'
            for name, url in stations:
                print(('{0:50s}'.format(name)))
        else:
            player.play(stream_url)
    elif args.pause:
        player.pause()
    elif args.mute:
        player.mute()
    elif args.stop:
        player.close()
    elif args.volume_up:
        player.volumeUp()
    elif args.volume_down:
        player.volumeDown()
    elif args.status:
        pass # Status is always displayed 
    else:
        for name, url in stations:
            print(('{0:50s} {1:s}'.format(name, url)))

    current = player.getCurrentStream()
    if current:
        print
        print 'Currently playing: %s' % current


if __name__ == '__main__':
    shell()
