import csv
import sys
import curses
import logging
from argparse import ArgumentParser
from os import path, getenv

from .radio import PyRadio
import iheart

PATTERN = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'


def __configureLogger():
    logger = logging.getLogger("pyradio")
    logger.setLevel(logging.DEBUG)

    # Handler
    fh = logging.FileHandler("pyradio.log")
    fh.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(PATTERN)

    # add formatter to ch
    fh.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(fh)

DEFAULT_FILE = ''
for p in [path.join(getenv('HOME', '~'), '.pyradio', 'stations.csv'),
          path.join(getenv('HOME', '~'), '.pyradio'),
          path.join(path.dirname(__file__), 'stations.csv')]:
    if path.exists(p) and path.isfile(p):
        DEFAULT_FILE = p
        break


def shell():
    parser = ArgumentParser(description="Console radio player")
    parser.add_argument("--stations",   "-s", default=DEFAULT_FILE, help="Path on stations csv file.")
    parser.add_argument("--play",       "-p", nargs='?', default=False, help="Start and play. The value is num station or empty for random.")
    parser.add_argument("--add",        "-a", action='store_true', help="Add station to list.")
    parser.add_argument("--remove",     "-r", nargs='?', default=False, help="Remove a station from the list.")
    parser.add_argument("--search",           action='store', default=None, help="Search iHeart for stations and display the results.")
    parser.add_argument("--search-add",       action='store', default=None, help="Search iHeart and add station to list.")
    parser.add_argument("--list",       "-l", action='store_true', help="List of added stations.")
    parser.add_argument("--debug",      "-d", action='store_true', help="Start pyradio in debug mode.")
    args = parser.parse_args()

    # No need to parse the file if we add station
    if args.add:
        params = raw_input("Enter the name: "), raw_input("Enter the url: ")
        with open(args.stations, 'a') as cfgfile:
            writter = csv.writer(cfgfile)
            writter.writerow(params)
            sys.exit(0)

    if args.remove:
        with open(args.stations, 'r') as cfgfile:
            reader = csv.reader(cfgfile)
            stations = [row for row in reader]

        remove_id = int(args.remove) - 1
        with open(args.stations, 'w+') as cfgfile:
            writter = csv.writer(cfgfile)
            for index, station in enumerate(stations):
                if remove_id != index:
                    writter.writerow(station)
        sys.exit(0)

    if args.search:
        read_file = iheart.search_cvs(args.search)
        reader = csv.reader(read_file)
        for row in reader:
            print row
        sys.exit(0)

    if args.search_add:
        read_file = iheart.search_cvs(args.search_add)
        reader = csv.reader(read_file)

        with open(args.stations, 'a') as cfgfile:
            writter = csv.writer(cfgfile)
            for row in reader:
                writter.writerow(row)
            sys.exit(0)

    with open(args.stations, 'r') as cfgfile:
        stations = []
        for row in csv.reader(filter(lambda row: row[0]!='#', cfgfile), skipinitialspace=True):
            name, url = [s.strip() for s in row]
            stations.append((name, url))

    if args.list:
        for name, url in stations:
            print(('{0:50s} {1:s}'.format(name, url)))
        sys.exit(0)

    if args.debug:
        __configureLogger()
        print("Debug mode acitvated")

    # Starts the radio gui.
    pyradio = PyRadio(stations, play=args.play)
    curses.wrapper(pyradio.setup)


if __name__ == '__main__':
    shell()
